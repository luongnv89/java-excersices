package test;

import java.util.ArrayList;

import object.Circle;
import object.Cube;
import object.Shapes;
import object.Sphere;
import object.Square;
import object.ThreeD;
import object.Triangle;
import object.TwoD;
// Commit nao
public class TestShapes {
	public static void main(String[] args) {
		ArrayList<Shapes> listShapes = new ArrayList<Shapes>();
		// TODO Auto-generated method stub
		Shapes cir = new Circle(4);
		listShapes.add(cir);
		// cir.WAY();
		// System.out.println("Area: " + ((Circle) cir).getArea() + "m2\n");
		Shapes squ = new Square(5, 6);
		listShapes.add(squ);
		// squ.WAY();
		// System.out.println("Area: " + squ.getArea() + "m2\n");
		Shapes tri = new Triangle(2, 3, 4);
		listShapes.add(tri);
		// tri.WAY();
		// System.out.println("Area: " + tri.getArea() + "m2\n");
		Shapes sph = new Sphere(4);
		listShapes.add(sph);
		// sph.WAY();
		// System.out.println("Volumn: " + sph.getVolumn() + "m3\n");
		Shapes cub = new Cube(2, 3, 4);
		listShapes.add(cub);
		// cub.WAY();
		// System.out.println("Volumn: " + cub.getVolumn() + "m3\n");

		for (int i = 0; i < listShapes.size(); i++) {
			if (listShapes.get(i).getD() == 2) {
				System.out.println("Area: "
						+ ((TwoD) listShapes.get(i)).getArea());
			} else
				System.out.println("Volumn: "
						+ ((ThreeD) listShapes.get(i)).getVolumn());
		}

	}
}
